<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Su contraseña fue restaurada!',
    'sent' => 'Te enviamos un e-mail con tu link de restauración de contraseña!',
    'throttled' => 'Por favor espere un poco antes de reintentar.',
    'token' => 'El token de restauración de contraseña en inválido.',
    'user' => "No encontramos ningún usuario con ese email.",

];
