<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'El campo :attribute debe ser aceptada.',
    'accepted_if' => 'El campo :attribute debe ser aceptado mientras el campo :other es :value.',
    'active_url' => 'El campo :attribute no es una URL válida.',
    'after' => 'El campo :attribute debe ser una fecha posterior a :date.',
    'after_or_equal' => 'El campo :attribute debe ser una fecha posterior o igual a :date.',
    'alpha' => 'El campo :attribute solo puede contener letras.',
    'alpha_dash' => 'El campo :attribute solo puede contener letras, números, barras and guiones bajos.',
    'alpha_num' => 'El campo :attribute solo puede contener letras o números.',
    'array' => 'El campo :attribute debe ser un array.',
    'before' => 'El campo :attribute debe ser una fecha anterior a :date.',
    'before_or_equal' => 'El campo :attribute debe ser una fecha anterior o igual :date.',
    'between' => [
        'array' => 'El campo :attribute debe poseer entre :min y :max items.',
        'file' => 'El campo :attribute debe pesar entre :min y :max kilobytes.',
        'numeric' => 'El campo :attribute debe ser un número entre :min y :max.',
        'string' => 'El campo :attribute debe poseer entre :min y :max caracteres.',
    ],
    'boolean' => 'El campo :attribute debe ser verdadero o falso.',
    'confirmed' => 'El campo confirmación de :attribute no coincide.',
    'current_password' => 'La contraseña es incorrecta.',
    'date' => 'El campo :attribute debe ser una fecha válida.',
    'date_equals' => 'El campo :attribute debe ser una fecha igual :date.',
    'date_format' => 'El campo :attribute debe ser en formato :format.',
    'declined' => 'El campo :attribute debe ser rechazado.',
    'declined_if' => 'El campo :attribute debe ser rechazado mientras el campo :other es :value.',
    'different' => 'El campo :attribute el y el campo :other deben ser distintos.',
    'digits' => 'El campo :attribute debe contener :digits dígitos.',
    'digits_between' => 'El campo :attribute debe tener entre :min y :max dígitos.',
    'dimensions' => 'El campo :attribute no posee una dimensión de imágen válida.',
    'distinct' => 'El campo :attribute posee un campo duplicado.',
    'email' => 'El campo :attribute debe ser un email válido.',
    'ends_with' => 'El campo :attribute debe finalizar con uno de los siguientes valores: :values.',
    'enum' => 'La selección :attribute es inválida.',
    'exists' => 'La selección :attribute es inválida.',
    'file' => 'El campo :attribute debe ser un archivo.',
    'filled' => 'El campo :attribute debe poseer un valor.',
    'gt' => [
        'array' => 'El campo :attribute debe tener mas de :value items.',
        'file' => 'El campo :attribute debe pesar mas de :value kilobytes.',
        'numeric' => 'El campo :attribute debe ser mayor que :value.',
        'string' => 'El campo :attribute debe tener mas de :value caracteres.',
    ],
    'gte' => [
        'array' => 'El campo :attribute deber poseer :value items o más.',
        'file' => 'El campo :attribute debe pesar :value kilobytes o más.',
        'numeric' => 'El campo :attribute deber ser mayor o igual :value.',
        'string' => 'El campo :attribute debe contener :value o más caracteres.',
    ],
    'image' => 'El campo :attribute debe ser una imagen.',
    'in' => 'La selección ingresada :attribute es inválida.',
    'in_array' => 'El campo :attribute no se encuentra dentro de :other.',
    'integer' => 'El campo :attribute debe ser un número entero.',
    'ip' => 'El campo :attribute debe ser una IP válida.',
    'ipv4' => 'El campo :attribute de ser una dirección IPv4 válida.',
    'ipv6' => 'El campo :attribute debe ser una dirección IPv6 válida.',
    'json' => 'El campo :attribute debe ser un string JSON válido.',
    'lt' => [
        'array' => 'El campo :attribute debe tener menos de :value items.',
        'file' => 'El campo :attribute debe pesar menos de :value kilobytes.',
        'numeric' => 'El campo :attribute debe ser menor que :value.',
        'string' => 'El campo :attribute debe poseer menos de :value caracteres.',
    ],
    'lte' => [
        'array' => 'The :attribute must not have more than :value items.',
        'file' => 'The :attribute must be less than or equal to :value kilobytes.',
        'numeric' => 'The :attribute must be less than or equal to :value.',
        'string' => 'The :attribute must be less than or equal to :value characters.',
    ],
    'mac_address' => 'The :attribute must be a valid MAC address.',
    'max' => [
        'array' => 'El campo :attribute no debe tener mas de :max items.',
        'file' => 'El campo :attribute no deber pesar mas de :max kilobytes.',
        'numeric' => 'El campo :attribute no debe ser mayor que :max.',
        'string' => 'El campo :attribute no debe poseer mas de :max caracteres.',
    ],
    'mimes' => 'El campo :attribute debe ser un archivo con extensión: :values.',
    'mimetypes' => 'El campo :attribute debe ser un archivo de tipo: :values.',
    'min' => [
        'array' => 'El campo :attribute debe tener al menos :min items.',
        'file' => 'El campo :attribute debe pesar al menos :min kilobytes.',
        'numeric' => 'El campo :attribute debe ser al menos :min.',
        'string' => 'El campo :attribute debe tener al menos :min caracteres.',
    ],
    'multiple_of' => 'El campo :attribute debe ser múltiplo de :value.',
    'not_in' => 'La selección ingresada :attribute es inválida.',
    'not_regex' => 'El campo :attribute no posee un formato válido.',
    'numeric' => 'El campo :attribute debe ser un número.',
    'present' => 'El campo :attribute debe estar presente.',
    'prohibited' => 'El campo :attribute está prohibido.',
    'prohibited_if' => 'El campo :attribute está prohibido mientras el campo :other es :value.',
    'prohibited_unless' => 'El campo :attribute es prohibido a menos que el campo :other posea :values.',
    'prohibits' => 'El campo :attribute prohíbe que :other esté presente.',
    'regex' => 'El campo :attribute tiene formato inválido.',
    'required' => 'El campo :attribute es obligatorio.',
    'required_array_keys' => 'El campo :attribute debe contener los siguientes datos: :values.',
    'required_if' => 'El campo :attribute es obligatorio mientras el campo :other es :value.',
    'required_unless' => 'El campo :attribute es requerido a menos que el campo :other sea :values.',
    'required_with' => 'El campo :attribute es oboligatorio mientras el campo :values está presente.',
    'required_with_all' => 'El campo :attribute es requerido mientras los campos :values están presentes.',
    'required_without' => 'El campo :attribute es obligatorio cuando los campos :values no están presentes.',
    'required_without_all' => 'El campo :attribute cuando ninguno de los campos :values están presentes.',
    'same' => 'El campo :attribute y el campo :other debe ser iguales.',
    'size' => [
        'array' => 'El campo :attribute debe contener :size items.',
        'file' => 'El campo :attribute debe pesar :size kilobytes.',
        'numeric' => 'El campo :attribute debe ser :size.',
        'string' => 'El campo :attribute debe tener :size caracteres.',
    ],
    'starts_with' => 'El campo :attribute debe comenzar con unos de los sigueinte valores: :values.',
    'string' => 'El campo :attribute debe ser un string válido.',
    'timezone' => 'El campo :attribute debe ser una zona horaria válida.',
    'unique' => 'Este :attribute ya está en uso.',
    'uploaded' => 'El campo :attribute falló al ser subido.',
    'url' => 'El campo :attribute de ser una URL válida.',
    'uuid' => 'El campo :attribute debe ser un UUID válido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
