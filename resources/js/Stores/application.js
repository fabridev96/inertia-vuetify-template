import { defineStore } from 'pinia'
import { Inertia } from "@inertiajs/inertia";

export const useApplicationStore = defineStore('applicationStore', {

    state: () => {
        return {
            showSidebar: false,
        }
    },
    actions: {
        toggleSidebar() {
            this.showSidebar = !this.showSidebar;
        },
        logout() {
            Inertia.post(route('logout'), {},{
                onSuccess: () => {
                    location.reload();
                }
            });
        }
    },
})
