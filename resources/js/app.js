import 'vuetify/styles';
import '@mdi/font/css/materialdesignicons.css';
import { createApp, h } from 'vue';
import { createInertiaApp } from '@inertiajs/inertia-vue3';
import { createVuetify } from 'vuetify';
import * as components from 'vuetify/components';
import * as directives from 'vuetify/directives';
import { createPinia } from 'pinia';
import MainLayout from './Layout/MainLayout';

require('./bootstrap');

const vuetify = createVuetify({
    components,
    directives
});

createInertiaApp({
  resolve: name => {
      const page = require(`./Pages/${name}`).default;
      page.layout = page.layout || MainLayout
      return page;
},
  setup({ el, App, props, plugin }) {
    createApp({ render: () => h(App, props) })
      .use(plugin)
      .use(createPinia())
      .use(vuetify)
      .mixin({ methods: { route } })
      .mount(el)
  },
});
